# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 16:04:28 2022

@author: amarf
"""

import pandas as pd 
import numpy as np 

df = pd.read_csv('data.csv')

df.head() #to see the first 5 lignes of the data

df.tail() #to see the last 5 lignes of the data

df.info() #to get info about the file such as the number of rows, the type of each data...

df.plot() #to get a plot with basic info, can also use the scatter option df.plot.scatter(x = '', y = '', s = )

df.mean() #to get the mean of all collumn