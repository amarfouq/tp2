# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 15:36:11 2022

@author: amarf
"""

def factorielle(n):
   if n == 0:
      return 1
   else:
      F = 1
      for k in range(2,n+1):
         F = F * k
      return F